package com.zippy.partner.common.swipe_button;

public interface OnActiveListener {
    void onActive();
}
