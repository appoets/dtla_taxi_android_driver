package com.zippy.partner.common.swipe_button;

public interface OnStateChangeListener {
    void onStateChange(boolean active);
}
