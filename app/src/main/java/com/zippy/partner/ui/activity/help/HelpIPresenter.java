package com.zippy.partner.ui.activity.help;


import com.zippy.partner.base.MvpPresenter;

public interface HelpIPresenter<V extends HelpIView> extends MvpPresenter<V> {

    void getHelp();
}
