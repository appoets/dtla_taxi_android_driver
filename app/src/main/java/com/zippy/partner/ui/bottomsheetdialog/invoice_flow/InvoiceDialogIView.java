package com.zippy.partner.ui.bottomsheetdialog.invoice_flow;

import com.zippy.partner.base.MvpView;

public interface InvoiceDialogIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
