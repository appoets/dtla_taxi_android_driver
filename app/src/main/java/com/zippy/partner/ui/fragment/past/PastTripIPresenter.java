package com.zippy.partner.ui.fragment.past;


import com.zippy.partner.base.MvpPresenter;

public interface PastTripIPresenter<V extends PastTripIView> extends MvpPresenter<V> {

    void getHistory();

}
