package com.zippy.partner.ui.activity.splash;

import com.zippy.partner.base.MvpPresenter;

public interface SplashIPresenter<V extends SplashIView> extends MvpPresenter<V> {

    void handlerCall();

    void getPlaces();

}
