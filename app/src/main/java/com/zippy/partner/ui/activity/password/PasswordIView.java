package com.zippy.partner.ui.activity.password;

import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.ForgotResponse;
import com.zippy.partner.data.network.model.User;

public interface PasswordIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);
    void onSuccess(User object);
    void onError(Throwable e);
}
