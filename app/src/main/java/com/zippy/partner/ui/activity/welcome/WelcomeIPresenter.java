package com.zippy.partner.ui.activity.welcome;

import com.zippy.partner.base.MvpPresenter;

public interface WelcomeIPresenter<V extends WelcomeIView> extends MvpPresenter<V> {
}
