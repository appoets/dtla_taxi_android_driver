package com.zippy.partner.ui.activity.setting;

import com.zippy.partner.base.MvpView;

public interface SettingsIView extends MvpView {

    void onSuccess(Object o);

    void onError(Throwable e);

}
