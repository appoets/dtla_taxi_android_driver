package com.zippy.partner.ui.activity.summary;


import com.zippy.partner.base.MvpPresenter;

public interface SummaryIPresenter<V extends SummaryIView> extends MvpPresenter<V> {

    void getSummary();
}
