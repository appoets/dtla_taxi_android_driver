package com.zippy.partner.ui.bottomsheetdialog.rating;

import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.Rating;

public interface RatingDialogIView extends MvpView {

    void onSuccess(Rating rating);
    void onError(Throwable e);
}
