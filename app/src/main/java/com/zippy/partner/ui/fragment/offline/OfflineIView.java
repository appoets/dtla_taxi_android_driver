package com.zippy.partner.ui.fragment.offline;

import com.zippy.partner.base.MvpView;

public interface OfflineIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
