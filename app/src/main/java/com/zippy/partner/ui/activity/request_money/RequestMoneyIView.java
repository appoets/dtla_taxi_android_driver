package com.zippy.partner.ui.activity.request_money;

import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.RequestDataResponse;

public interface RequestMoneyIView extends MvpView {

    void onSuccess(RequestDataResponse response);
    void onSuccess(Object response);
    void onError(Throwable e);

}
