package com.zippy.partner.ui.activity.add_card;

import com.zippy.partner.base.MvpView;

public interface AddCardIView extends MvpView {

    void onSuccess(Object card);
    void onError(Throwable e);
}
