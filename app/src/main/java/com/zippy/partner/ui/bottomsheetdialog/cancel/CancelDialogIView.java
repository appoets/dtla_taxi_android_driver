package com.zippy.partner.ui.bottomsheetdialog.cancel;

import com.zippy.partner.base.MvpView;

public interface CancelDialogIView extends MvpView {

    void onSuccessCancel(Object object);
    void onError(Throwable e);
}
