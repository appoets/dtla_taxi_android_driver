package com.zippy.partner.ui.activity.earnings;


import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.EarningsList;

public interface EarningsIView extends MvpView {

    void onSuccess(EarningsList earningsLists);
    void onError(Throwable e);
}
