package com.zippy.partner.ui.activity.main;

import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.InitSettingsResponse;
import com.zippy.partner.data.network.model.TripResponse;
import com.zippy.partner.data.network.model.UserResponse;

public interface MainIView extends MvpView {
    void onSuccess(UserResponse user);

    void onError(Throwable e);

    void onSuccessLogout(Object object);

    void onSuccess(TripResponse tripResponse);

    void onSuccessProviderAvailable(Object object);

    void onSuccessFCM(Object object);

    void onSuccessLocationUpdate(TripResponse tripResponse);

    void onSuccess(InitSettingsResponse initSettingsResponse);


}
