package com.zippy.partner.ui.activity.email;

import com.zippy.partner.base.MvpPresenter;

public interface EmailIPresenter<V extends EmailIView> extends MvpPresenter<V> {
}
