package com.zippy.partner.ui.activity.upcoming_detail;


import com.zippy.partner.base.MvpPresenter;

public interface UpcomingTripDetailIPresenter<V extends UpcomingTripDetailIView> extends MvpPresenter<V> {

    void getUpcomingDetail(String request_id);

}
