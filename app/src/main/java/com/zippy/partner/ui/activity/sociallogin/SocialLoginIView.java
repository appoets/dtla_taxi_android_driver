package com.zippy.partner.ui.activity.sociallogin;

import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.Token;

public interface SocialLoginIView extends MvpView {

    void onSuccess(Token token);
    void onError(Throwable e);
}
