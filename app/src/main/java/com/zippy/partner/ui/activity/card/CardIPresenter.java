package com.zippy.partner.ui.activity.card;

import com.zippy.partner.base.MvpPresenter;

public interface CardIPresenter<V extends CardIView> extends MvpPresenter<V> {

    void deleteCard(String cardId);
    void card();
    void changeCard(String cardId);
}
