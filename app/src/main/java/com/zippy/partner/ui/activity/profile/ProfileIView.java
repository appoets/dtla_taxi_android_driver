package com.zippy.partner.ui.activity.profile;

import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.UserResponse;

public interface ProfileIView extends MvpView {

    void onSuccess(UserResponse user);
    void onSuccessUpdate(UserResponse object);
    void onError(Throwable e);

}
