package com.zippy.partner.ui.activity.reset_password;

import com.zippy.partner.base.MvpView;

public interface ResetIView extends MvpView{

    void onSuccess(Object object);
    void onError(Throwable e);
}
