package com.zippy.partner.ui.bottomsheetdialog.cancel;

import com.zippy.partner.base.BasePresenter;
import com.zippy.partner.data.network.APIClient;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CancelDialogPresenter<V extends CancelDialogIView> extends BasePresenter<V> implements CancelDialogIPresenter<V> {


    @Override
    public void cancelRequest(HashMap<String, Object> obj) {
        getCompositeDisposable().add(
                APIClient
                        .getAPIClient()
                        .cancelRequest(obj)
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                trendsResponse -> getMvpView().onSuccessCancel(trendsResponse),
                                throwable -> getMvpView().onError(throwable)
                        )
        );
    }
}
