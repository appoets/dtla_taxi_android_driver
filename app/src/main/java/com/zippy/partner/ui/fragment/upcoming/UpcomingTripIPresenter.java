package com.zippy.partner.ui.fragment.upcoming;


import com.zippy.partner.base.MvpPresenter;

public interface UpcomingTripIPresenter<V extends UpcomingTripIView> extends MvpPresenter<V> {

    void getUpcoming();

}
