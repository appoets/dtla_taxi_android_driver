package com.zippy.partner.ui.activity.summary;


import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.Summary;

public interface SummaryIView extends MvpView {

    void onSuccess(Summary object);
    void onError(Throwable e);
}
