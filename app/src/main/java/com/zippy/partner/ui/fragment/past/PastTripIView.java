package com.zippy.partner.ui.fragment.past;


import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.HistoryList;

import java.util.List;

public interface PastTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
