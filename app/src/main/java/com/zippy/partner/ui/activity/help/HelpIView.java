package com.zippy.partner.ui.activity.help;

import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help object);
    void onError(Throwable e);
}
