package com.zippy.partner.ui.activity.forgot_password;

import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.ForgotResponse;

public interface ForgotIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);
    void onError(Throwable e);
}
