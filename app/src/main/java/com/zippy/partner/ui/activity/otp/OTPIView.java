package com.zippy.partner.ui.activity.otp;


import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.MyOTP;

public interface OTPIView extends MvpView {
    void onSuccess(MyOTP otp);
    void onError(Throwable e);
}
