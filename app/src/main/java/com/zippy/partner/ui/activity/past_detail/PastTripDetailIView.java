package com.zippy.partner.ui.activity.past_detail;


import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.HistoryDetail;

public interface PastTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
