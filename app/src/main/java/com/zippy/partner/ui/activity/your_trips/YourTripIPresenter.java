package com.zippy.partner.ui.activity.your_trips;

import com.zippy.partner.base.MvpPresenter;

public interface YourTripIPresenter<V extends YourTripIView> extends MvpPresenter<V> {
}
