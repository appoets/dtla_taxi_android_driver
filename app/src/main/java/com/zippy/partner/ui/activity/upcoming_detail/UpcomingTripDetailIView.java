package com.zippy.partner.ui.activity.upcoming_detail;


import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.HistoryDetail;

public interface UpcomingTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
