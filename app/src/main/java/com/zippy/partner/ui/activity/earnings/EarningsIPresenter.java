package com.zippy.partner.ui.activity.earnings;


import com.zippy.partner.base.MvpPresenter;

public interface EarningsIPresenter<V extends EarningsIView> extends MvpPresenter<V> {

    void getEarnings();
}
