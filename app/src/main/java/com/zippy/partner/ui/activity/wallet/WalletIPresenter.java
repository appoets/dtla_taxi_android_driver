package com.zippy.partner.ui.activity.wallet;

import com.zippy.partner.base.MvpPresenter;

public interface WalletIPresenter<V extends WalletIView> extends MvpPresenter<V> {

    void getWalletData();
}
