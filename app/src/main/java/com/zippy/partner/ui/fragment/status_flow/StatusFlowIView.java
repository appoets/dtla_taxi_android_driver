package com.zippy.partner.ui.fragment.status_flow;

import com.zippy.partner.base.MvpView;

public interface StatusFlowIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
