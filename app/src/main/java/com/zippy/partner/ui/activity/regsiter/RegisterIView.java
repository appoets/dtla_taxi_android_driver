package com.zippy.partner.ui.activity.regsiter;

import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.MyOTP;
import com.zippy.partner.data.network.model.User;

public interface RegisterIView extends MvpView {

    void onSuccess(User user);
    void onSuccess(Object verifyEmail);
    void onSuccess(MyOTP otp);
    void onError(Throwable e);

}
