package com.zippy.partner.ui.activity.wallet;

import com.zippy.partner.base.MvpView;
import com.zippy.partner.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {

    void onSuccess(WalletResponse response);
    void onError(Throwable e);
}
