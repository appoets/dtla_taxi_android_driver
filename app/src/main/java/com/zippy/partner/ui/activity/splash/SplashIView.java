package com.zippy.partner.ui.activity.splash;

import com.zippy.partner.base.MvpView;

public interface SplashIView extends MvpView {


    void redirectHome();

    void onSuccess(Object user);
    void onError(Throwable e);
}
